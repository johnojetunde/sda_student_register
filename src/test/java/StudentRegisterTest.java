import com.sda.java.advanced.Country;
import com.sda.java.advanced.CountryPrinter;
import com.sda.java.advanced.Georgia;
import com.sda.java.advanced.Philippine;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class StudentRegisterTest {
    @Test
    void add() {
        StudentRegister register = new StudentRegister();
        register.add("studentName");

        assertThat(register.size()).isEqualTo(1);

        register.add("secondStudent");
        assertThat(register.size()).isEqualTo(2);
    }

    @Test
    void getRegister() {
        StudentRegister register = new StudentRegister();
        register.add("Jasher");
        register.add("Nico D.");

        List<String> students = register.get();
        assertThat(students).containsExactly("Jasher", "Nico D.");
    }

    @Test
    void add_withoutDuplicate() {
        StudentRegister register = new StudentRegister();
        register.add("Jan");
        register.add("Jaime");
        register.add("Jan");

        assertThat(register.size()).isEqualTo(2);
        assertThat(register.get()).containsExactly("Jan", "Jaime");
    }

    @Test
    void remove_student() {
        StudentRegister register = new StudentRegister();
        register.add("Roylan");
        register.add("Jason");
        register.add("Jasher");

        register.remove("Jasher");

        assertThat(register.size()).isEqualTo(2);
        assertThat(register.get()).containsExactly("Roylan", "Jason");
    }

    @Test
    void printCountry() {
        Country philippine = new Philippine("Philipine", "location", "Europe");
        Philippine philippine2 = new Philippine("Philipine", "location", "Europe");
        Country georgia = new Georgia("Georgia", "location", "Europe");
        Georgia georgia2 = new Georgia("Georgia", "location", "Europe");

        CountryPrinter<Philippine> philippine1 = new CountryPrinter<>(philippine2);
        CountryPrinter<Philippine> philippine2Printer = new CountryPrinter<>(philippine2);
        CountryPrinter<Country> georgiaPrinter = new CountryPrinter<>(georgia);
        CountryPrinter<Georgia> georgia2Printer = new CountryPrinter<>(georgia2);

        philippine1.print();
        philippine2Printer.print();
        georgiaPrinter.print();
        georgia2Printer.print();
    }

    @Test
    void exceptionHandler_exception() {
        try {
            throwException();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Always run this");
        }
    }


    @Test
    void exceptionHandler_runtime() {
        assertThatThrownBy(this::throwRuntimeException)
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Things are not working");

        System.out.println("continue the code");
    }

    private void throwRuntimeException() {
        throw new RuntimeException("Things are not working");
    }

    private void throwException() throws Exception {
        throw new Exception("Things are not working");
    }
}
