import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Using TDD approach, create a system that keeps track of the places the students have visited
 * The places each student visit should be unique (i.e if a student has visited a place before, it should not be added to
 * list of the places that the student have visited).
 * <p>
 * We should be able to add a place
 * We should be able to get all the places for a given student
 * We should be able to get all the unique places all the students have been
 */
public class StudentPlacesTest {
    private StudentPlaces studentPlaces;

    @BeforeEach
    void setUp() {
        studentPlaces = new StudentPlaces();
    }

    @Test
    void addPlace() {
        studentPlaces.addPlace("Jasher", "Office");

        assertThat(studentPlaces.size()).isEqualTo(1);
        assertThat(studentPlaces.get("Jasher")).containsExactlyInAnyOrder("Office");
    }

    @Test
    void addMultiplePlaces() {
        studentPlaces.addPlace("Jaime", "Training");
        studentPlaces.addPlace("Jaime", "Restaurant");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("Json", "Library");

        assertThat(studentPlaces.size()).isEqualTo(2);
        assertThat(studentPlaces.get("Jaime")).containsExactlyInAnyOrder("Training", "Restaurant");
        assertThat(studentPlaces.get("Json")).containsExactlyInAnyOrder("Library");
    }

    @Test
    void getAllPlaces() {
        studentPlaces.addPlace("Jaime", "Training");
        studentPlaces.addPlace("Jaime", "Restaurant");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("John", "Library");
        studentPlaces.addPlace("John", "Restaurant");

        assertThat(studentPlaces.size()).isEqualTo(3);
        assertThat(studentPlaces.get("Jaime")).containsExactlyInAnyOrder("Training", "Restaurant");
        assertThat(studentPlaces.get("Json")).containsExactlyInAnyOrder("Library");
        assertThat(studentPlaces.get("John")).containsExactlyInAnyOrder("Library", "Restaurant");
        assertThat(studentPlaces.getPlaces()).containsExactlyInAnyOrder("Training", "Restaurant", "Library");
    }

    @Test
    void getAllStudents() {
        studentPlaces.addPlace("Jaime", "Training");
        studentPlaces.addPlace("Jaime", "Restaurant");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("John", "Library");
        studentPlaces.addPlace("John", "Restaurant");

        assertThat(studentPlaces.size()).isEqualTo(3);
        assertThat(studentPlaces.get("Jaime")).containsExactlyInAnyOrder("Training", "Restaurant");
        assertThat(studentPlaces.get("Json")).containsExactlyInAnyOrder("Library");
        assertThat(studentPlaces.get("John")).containsExactlyInAnyOrder("Library", "Restaurant");
        assertThat(studentPlaces.getPlaces()).containsExactlyInAnyOrder("Training", "Restaurant", "Library");
        assertThat(studentPlaces.getStudents()).containsExactlyInAnyOrder("Jaime", "Json", "John");
    }

    @Test
    void getAllStudents_sorted() {
        studentPlaces.addPlace("Jaime", "Training");
        studentPlaces.addPlace("Jaime", "Restaurant");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("Json", "Library");
        studentPlaces.addPlace("John", "Library");
        studentPlaces.addPlace("John", "Restaurant");

        assertThat(studentPlaces.size()).isEqualTo(3);
        assertThat(studentPlaces.get("Jaime")).containsExactly("Restaurant", "Training");
        assertThat(studentPlaces.get("Json")).containsExactly("Library");
        assertThat(studentPlaces.get("John")).containsExactly("Library", "Restaurant");
        assertThat(studentPlaces.getPlaces()).containsExactly("Library", "Restaurant", "Training");
        assertThat(studentPlaces.getStudents()).containsExactly("Jaime", "John", "Json");
    }

    private static class StudentPlaces {
        private final Map<String, Set<String>> record = new HashMap<>();

        public void addPlace(String name, String place) {
            Set<String> places = record.getOrDefault(name, new HashSet<>());
            places.add(place);
            record.put(name, places);
        }

        public int size() {
            return record.size();
        }

        public Set<String> get(String studentName) {
            return sort(record.getOrDefault(studentName, emptySet()));
        }

        public Set<String> getPlaces() {
            Set<String> uniquePlaces = new HashSet<>();

            for (Entry<String, Set<String>> entry : record.entrySet()) {
                uniquePlaces.addAll(entry.getValue());
            }
            return sort(uniquePlaces);
        }

        public Set<String> getStudents() {
            Set<String> uniqueStudents = new HashSet<>();

            for (Entry<String, Set<String>> entry : record.entrySet()) {
                uniqueStudents.add(entry.getKey());
            }
            return sort(uniqueStudents);
        }

        private Set<String> sort(Set<String> elements) {
            System.out.println("Before sorting " + elements);
            Set<String> sortedElements = elements.stream().sorted()
                    .collect(Collectors.toCollection(LinkedHashSet::new));

            System.out.println("After sorting " + sortedElements);
            return sortedElements;
        }
    }
}
