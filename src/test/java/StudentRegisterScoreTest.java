import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class StudentRegisterScoreTest {
    /**
     * Set, List, Map
     */
    @Test
    void add_student() {
        StudentRegisterScore studentScore = new StudentRegisterScore();
        studentScore.addScore("Jasher", 100.00);

        assertThat(studentScore.size()).isEqualTo(1);
        assertThat(studentScore.getScore("Jasher")).isEqualTo(100.00);
    }

    @Test
    void add_scoreMultipleTimes() {
        StudentRegisterScore studentScore = new StudentRegisterScore();
        studentScore.addScore("Jasher", 100.00);
        studentScore.addScore("Jasher", 100.00);
        studentScore.addScore("Json", 50.00);

        assertThat(studentScore.size()).isEqualTo(2);
        assertThat(studentScore.getScore("Jasher")).isEqualTo(200.00);
        assertThat(studentScore.getScore("Json")).isEqualTo(50.00);
    }

    @Test
    void reduce_scoreMultipleTimes() {
        StudentRegisterScore studentScore = new StudentRegisterScore();
        studentScore.addScore("Jasher", 100.00);
        studentScore.addScore("Jasher", 100.00);
        studentScore.addScore("Json", 50.00);
        studentScore.reduceScore("Json", 20.00);

        assertThat(studentScore.size()).isEqualTo(2);
        assertThat(studentScore.getScore("Jasher")).isEqualTo(200.00);
        assertThat(studentScore.getScore("Json")).isEqualTo(30.00);
    }

    @Test
    void reduce_scoreUnregisteredUser() {
        StudentRegisterScore studentScore = new StudentRegisterScore();
        assertThatThrownBy(() -> studentScore.reduceScore("Json", 20.00))

                .isInstanceOf(RuntimeException.class)
                .hasMessage("You need to be registered first");
    }

    @Test
    void reduce_scoreInvalidAmount() {
        StudentRegisterScore studentScore = new StudentRegisterScore();
        studentScore.addScore("Danica", 40.0);

        assertThatThrownBy(() -> studentScore.reduceScore("Danica", 41.00))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("User current score is 40.0");
    }


    private static class StudentRegisterScore {
        private final Map<String, Double> records = new HashMap<>();

        private void register(String name) {
            records.putIfAbsent(name, 0.0);
        }

        public void addScore(String name, double score) {
            register(name);
            Double currentScore = getScore(name) + score;
            records.put(name, currentScore);
        }

        public void reduceScore(String name, Double score) {
            if (!records.containsKey(name)) {
                throw new RuntimeException("You need to be registered first");
            }

            Double currentScore = getScore(name);
            if (currentScore < score) {
                throw new RuntimeException("User current score is " + currentScore);
            }

            Double finalScore = currentScore - score;
            records.put(name, finalScore);
        }

        public Integer size() {
            return records.size();
        }

        public Double getScore(String name) {
            return records.get(name);
        }
    }


    /**
     * Using TDD approach, create a system that keeps track of the places the students have visited
     * The places each student visit should be unique (i.e if a student has visited a place before, it should not be added to
     * list of the places that the student have visited).
     *
     * We should be able to add a place
     * We should be able to print all the places for a given student
     * We should be able to print all the unique places all the students have been
     *
     *
     *
     *
     * */
}
