package com.sda.java.advance.wallet;

import com.sda.java.advanced.wallet.InsufficientBalanceException;
import com.sda.java.advanced.wallet.TransactionType;
import com.sda.java.advanced.wallet.Wallet;
import com.sda.java.advanced.wallet.WalletHistory;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WalletTest {
    /**
     * Create a thread-safe wallet system.
     * You should be able to perform the following functions on the wallet system:
     * 1. Transfer between each wallet
     * 2. Get balance of each wallet
     * 3. Get the transfer records of each wallet
     */

    @Test
    void getWalletBalance_emptyWallet() {
        Wallet wallet = new Wallet();
        assertEquals(wallet.getBalance(), BigDecimal.ZERO);
    }

    @Test
    void getWalletBalance_usingFilledWallet() {
        Wallet wallet = new Wallet(30.0);
        assertEquals(wallet.getBalance(), BigDecimal.valueOf(30.0));
    }

    @Test
    void creditWallet() {
        Wallet wallet = new Wallet(20.0);
        assertEquals(wallet.getBalance(), BigDecimal.valueOf(20.0));
        wallet.creditWallet(20.0);
        assertEquals(wallet.getBalance(), BigDecimal.valueOf(40.0));
    }

    @Test
    void debitWallet() {
        Wallet wallet = new Wallet(20.0);
        assertEquals(wallet.getBalance(), BigDecimal.valueOf(20.0));

        wallet.debitWallet(8.10);
        assertEquals(wallet.getBalance(), BigDecimal.valueOf(11.90));
    }

    @Test
    void debitWallet_insufficientFunds() {
        Wallet wallet = new Wallet(10.0);

        assertThrows(InsufficientBalanceException.class, () -> wallet.debitWallet(11.0));
    }

    @Test
    void creditWallet_getWalletHistory() {
        Wallet wallet = new Wallet(20.0);

        List<WalletHistory> history = wallet.getHistory();
        assertEquals(history.size(), 1);
        assertEquals(history.get(0).getType(), TransactionType.CREDIT);
        assertEquals(history.get(0).getPreviousBalance(), BigDecimal.ZERO);
        assertEquals(history.get(0).getCurrentBalance(), BigDecimal.valueOf(20.0));
        assertEquals(history.get(0).getDescription(), "Wallet credit");
    }

    @Test
    void debitWallet_getWalletHistory() {
        Wallet wallet = new Wallet(30.0);
        wallet.debitWallet(10.0);
        List<WalletHistory> history = wallet.getHistory();

        assertEquals(history.size(), 2);
        assertEquals(history.get(1).getType(), TransactionType.DEBIT);
        assertEquals(history.get(1).getPreviousBalance(), BigDecimal.valueOf(30.0));
        assertEquals(history.get(1).getCurrentBalance(), BigDecimal.valueOf(20.0));
        assertEquals(history.get(1).getDescription(), "Wallet debit");
    }

    @Test
    void transferMoney() {
        Wallet walletA = new Wallet(30.0);
        Wallet walletB = new Wallet(5.0);

        walletA.transfer(10.0, walletB);
        assertEquals(walletA.getBalance(), BigDecimal.valueOf(20.0));
        assertEquals(walletB.getBalance(), BigDecimal.valueOf(15.0));

        List<WalletHistory> walletAHistory = walletA.getHistory();
        List<WalletHistory> walletBHistory = walletB.getHistory();

        assertEquals(walletAHistory.size(), 2);
        assertEquals(walletBHistory.size(), 2);
        assertEquals(walletAHistory.get(1).getType(), TransactionType.DEBIT);
        assertEquals(walletBHistory.get(1).getType(), TransactionType.CREDIT);
        assertEquals(walletAHistory.get(1).getDescription(), "Wallet debit via transfer");
        assertEquals(walletBHistory.get(1).getDescription(), "Wallet credit via transfer");
    }

    @Test
    void transferMoney_insufficientFunds() {
        Wallet walletA = new Wallet(30.0);
        Wallet walletB = new Wallet(20.0);

        assertThrows(InsufficientBalanceException.class, () -> walletB.transfer(40, walletA));
        assertEquals(walletA.getBalance(), BigDecimal.valueOf(30.0));
        assertEquals(walletB.getBalance(), BigDecimal.valueOf(20.0));
    }
}
