package com.sda.java.advanced;

public abstract class Country {
    private Object flag;
    private String name;
    private Object location;
    private String continent;

    public Country(String name, Object location, String continent) {
        this.name = name;
        this.location = location;
        this.continent = continent;
    }

    Object getFlag() {
        return null;
    }

    public String getName() {
        return name;
    }

    public Object getLocation() {
        return location;
    }

    public String getContinent() {
        return continent;
    }

    @Override
    public String toString() {
        return "Country{" +
                "flag=" + flag +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", continent='" + continent + '\'' +
                '}';
    }
}
