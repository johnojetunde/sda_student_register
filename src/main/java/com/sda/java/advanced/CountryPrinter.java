package com.sda.java.advanced;

import java.util.List;

public class CountryPrinter<T extends Country> {
    private final T country;

    public CountryPrinter(T country) {
        this.country = country;
    }

    public void print() {
        System.out.println(country);
    }


    /**
     * <T extends Country>  -  the object passed must be a child of country
     * <T> -  the object passed in must be an instance of country
     * <p>
     * Generic Methods:
     * <?> - wildcard (any object)
     * <? super Integer> - any Object that has a sub-type Integer (Integer,Number,Object)
     * <? extends Number> -  any Object that has a super type Number(Integer,Number)
     */
    public void doSomething(List<? super Integer> input) {
    }

    /**
     * Create 2 generic classes.
     * 1. Generic class should support all instances or sub types of <T>
     * 2. Generic class should only support instances of <T>
     *
     * Create 3 generic methods
     * 1. generic method that takes in any object that is a subtype of BigDecimal
     * 2. generic method that takes in any object that is a super type of Integer
     * 3. generic method that takes in any object <?>
     * */

}
