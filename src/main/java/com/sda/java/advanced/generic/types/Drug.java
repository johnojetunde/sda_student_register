package com.sda.java.advanced.generic.types;

public class Drug extends MedicinalLiquid {
    public Drug(String name, String type) {
        super(name, type);
    }
}
