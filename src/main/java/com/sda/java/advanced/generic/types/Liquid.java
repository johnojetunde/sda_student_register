package com.sda.java.advanced.generic.types;

public class Liquid {
    private final String name;
    private final String type;

    public Liquid(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Liquid{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
