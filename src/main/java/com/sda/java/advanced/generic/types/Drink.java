package com.sda.java.advanced.generic.types;

public class Drink<T extends Liquid> {
    private final T liquid;

    public Drink(T liquid) {
        this.liquid = liquid;
    }

    public void drink() {
        System.out.println(liquid);
        //pour the drink in your mouth
    }

    public void measure() {
        //hthr
    }
}
