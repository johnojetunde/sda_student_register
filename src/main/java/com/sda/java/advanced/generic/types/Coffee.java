package com.sda.java.advanced.generic.types;

public class Coffee extends Liquid {
    public Coffee(String name, String type) {
        super(name, type);
    }
}
