package com.sda.java.advanced.generic.types;

public class MedicinalLiquid extends Liquid {
    public MedicinalLiquid(String name, String type) {
        super(name, type);
    }
}
