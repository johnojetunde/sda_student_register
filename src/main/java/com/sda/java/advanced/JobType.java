package com.sda.java.advanced;

public enum JobType {
    FREELANCE,
    PART_TIME,
    FULL_TIME
}
