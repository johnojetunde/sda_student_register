package com.sda.java.advanced.com.sda.java.advanced;

public interface Vehicle {
    void start();

    void stop();

    String getLicense();

    String getModel();
}
