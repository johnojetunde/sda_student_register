package com.sda.java.advanced.com.sda.java.advanced;

public class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("Start the car in it's own way");
    }

    @Override
    public void stop() {
        System.out.println("stop the car by switching off the ignition");
    }

    @Override
    public String getLicense() {
        return "00111CAR111111";
    }

    @Override
    public String getModel() {
        return "CAR-BMW";
    }
}
