package com.sda.java.advanced.com.sda.java.advanced;

public class Truck implements Vehicle {
    @Override
    public void start() {
        System.out.println("start the truck by engaging the gear and the ignition");
    }

    @Override
    public void stop() {
        System.out.println("Just remove the key from the ignition");
    }

    @Override
    public String getLicense() {
        return "001Truck";
    }

    @Override
    public String getModel() {
        return "TRUCK-Honda";
    }
}
