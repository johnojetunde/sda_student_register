package com.sda.java.advanced.com.sda.java.advanced;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

public class SummaryMain {
    /**
     * CompletableFuture - done
     * Future - done
     * Atomics -  done
     * interface -  done
     * anonymous class - done
     * Generic Types  ---- maybe
     * block synchronization vs method synchronization -  done
     * callable - done
     * runnable - done
     * <p>
     *
     *
     * Create an Employee Salary Management System
     * 1. Add new employee
     * 2. Set employee salary
     * 3. Employee should have JobRole
     * 4. Each JobRole should have salary and type of role
     * 5. You can only increase/decrease salary for the each role and once the salary of a role is increase,
     *every employee with that role would have the new amount as their salary
     * 6. fire employee
     * 7. calculate total salary you are paying to all active employees
     * 8. We should be able to see fired employees
     */
    public static void main(String... args) {
        System.out.println(Thread.currentThread().getName());
        Runnable runnable = () -> System.out.println(Thread.currentThread().getName());

        Thread newThread = new Thread(runnable);
        whileMultiplication();
        whileMultiplication2();
        newThread.start();

        Car car = new Car();
        Truck truck = new Truck();

        printLicense(car);
        printLicense(truck);

    }

    private static void printLicense(Vehicle vehicle) {
        System.out.println(vehicle.getLicense());
    }

    private static Future<?> whileMultiplication() {
        return newSingleThreadExecutor()
                .submit(() -> {
                    System.out.println(Thread.currentThread().getName());
                });
    }

    private static CompletableFuture<?> whileMultiplication2() {
        return CompletableFuture.runAsync(
                () -> System.out.println(Thread.currentThread().getName())
        );
    }

    private synchronized int calculateHash() {
        return 0;
    }

    private void calculateHash2() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                System.out.println(i);
            }
            {

            }

        }
    }


}
