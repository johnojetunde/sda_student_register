package com.sda.java.advanced;

import com.sda.java.advanced.generic.types.*;

import java.util.List;

public class Main {

    //Optional parameter but it can list of parameters
    public static void main(String... args) {
        Coffee coffee = new Coffee("Expresso", "L");
        Drug drug = new Drug("Herbal", "X");
        MedicinalLiquid medicinalLiquid = new Drug("Herbal", "X");

        Drink<Coffee> coffeeDrink = new Drink<>(coffee);
        Drink<MedicinalLiquid> medicinalLiquidDrink = new Drink<>(medicinalLiquid);
        Drink<Drug> drugDrink = new Drink<>(drug);

        Action.doNothing();
        Action.doNothing("name", "ncdjfj", "hdhdfhfh", "jk");

        Action.workWithMedicinalLiquids(List.of(medicinalLiquid));
    }

    private static class Action {

        public static <T extends Liquid> void workWithLiquids(T liquid) {

        }

        public static void workWithDrugs(List<? super Drug> drugs) {

        }

        public static void doNothing(String... names) {

        }

        public static void doNothings(String[] names) {

        }

        public static void workWithMedicinalLiquids(List<? extends MedicinalLiquid> liquid) {

        }
    }
}
