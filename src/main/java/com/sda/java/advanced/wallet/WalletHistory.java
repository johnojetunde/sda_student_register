package com.sda.java.advanced.wallet;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WalletHistory {
    private String description;
    private TransactionType type;
    private BigDecimal previousBalance;
    private BigDecimal transactionAmount;
    private BigDecimal currentBalance;
    private LocalDateTime createdDate;

    public WalletHistory(String description,
                         TransactionType type,
                         BigDecimal previousBalance,
                         BigDecimal currentBalance,
                         BigDecimal transactionAmount,
                         LocalDateTime createdDate) {
        this.description = description;
        this.type = type;
        this.previousBalance = previousBalance;
        this.currentBalance = currentBalance;
        this.createdDate = createdDate;
        this.transactionAmount = transactionAmount;
    }

    public String getDescription() {
        return description;
    }

    public TransactionType getType() {
        return type;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    @Override
    public String toString() {
        return "WalletHistory{" +
                "description='" + description + '\'' +
                ", type=" + type +
                ", previousBalance=" + previousBalance +
                ", currentBalance=" + currentBalance +
                ", transactionAmount=" + transactionAmount +
                ", createdDate=" + createdDate +
                '}';
    }
}
