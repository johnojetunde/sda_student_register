package com.sda.java.advanced.wallet;

public class WalletMain {
    public static void main(String... args) {
        Wallet roylanWallet = new Wallet("Roylan");
        Wallet jaimeWallet = new Wallet("Jaime");
        roylanWallet.creditWallet(20);
        roylanWallet.creditWallet(10);
        jaimeWallet.creditWallet(5);
        roylanWallet.transfer(20, jaimeWallet);

        System.out.println(roylanWallet);
        System.out.println(jaimeWallet);
    }
}
