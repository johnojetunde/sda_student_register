package com.sda.java.advanced.wallet;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Wallet {
    private final AtomicReference<BigDecimal> balance = new AtomicReference<>(BigDecimal.ZERO);
    private final List<WalletHistory> history = new ArrayList<>();
    private String name = "";

    public Wallet() {
    }

    public Wallet(String name) {
        this.name = name;
    }

    public Wallet(double v) {
        creditWallet(v);
    }

    synchronized void creditWallet(double v, String description) {
        BigDecimal currentBalanceBeforeCredit = balance.get();
        BigDecimal creditValue = BigDecimal.valueOf(v);
        BigDecimal updatedValue = currentBalanceBeforeCredit.add(creditValue);
        setBalance(updatedValue);
        newHistory(
                description,
                TransactionType.CREDIT,
                currentBalanceBeforeCredit,
                updatedValue,
                creditValue);
    }

    public void creditWallet(double v) {
        creditWallet(v, "Wallet credit");
    }

    public BigDecimal getBalance() {
        return balance.get();
    }

    private synchronized void setBalance(BigDecimal updatedValue) {
        balance.set(updatedValue);
    }

    private void newHistory(String description,
                            TransactionType type,
                            BigDecimal previousBalance,
                            BigDecimal currentBalance,
                            BigDecimal transactionAmount) {
        LocalDateTime createdDate = LocalDateTime.now();
        WalletHistory walletHistory = new WalletHistory(
                description,
                type,
                previousBalance,
                currentBalance,
                transactionAmount,
                createdDate);
        history.add(walletHistory);
    }

    synchronized void debitWallet(double v, String description) {
        BigDecimal currentBalanceBeforeDebit = balance.get();
        BigDecimal debitValue = BigDecimal.valueOf(v);
        if (!isBalanceSufficient(currentBalanceBeforeDebit, debitValue)) {
            throw new InsufficientBalanceException("Insufficient Balance");
        }

        BigDecimal updatedValue = currentBalanceBeforeDebit.subtract(debitValue);
        setBalance(updatedValue);
        newHistory(
                description,
                TransactionType.DEBIT,
                currentBalanceBeforeDebit,
                updatedValue,
                debitValue);
    }

    public void debitWallet(double v) {
        debitWallet(v, "Wallet debit");
    }

    private boolean isBalanceSufficient(BigDecimal balanceNow, BigDecimal debitValue) {
        return balanceNow.compareTo(debitValue) >= 0;
    }

    public List<WalletHistory> getHistory() {
        return history;
    }

    public void transfer(double v, Wallet walletB) {
        debitWallet(v, "Wallet debit via transfer");
        walletB.creditWallet(v, "Wallet credit via transfer");

    }

    @Override
    public String toString() {
        return "Wallet{" +
                "name=" + name +
                ",balance=" + balance.get() +
                ", history=" + history +
                '}';
    }
}

