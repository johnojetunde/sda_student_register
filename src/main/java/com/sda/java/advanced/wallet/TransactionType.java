package com.sda.java.advanced.wallet;

public enum TransactionType {
    CREDIT,
    DEBIT
}
