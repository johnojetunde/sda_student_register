package com.sda.java.advanced.concurrency;

public class Main {
    public static void main(String... args) {

        System.out.println("This is a current thread " + Thread.currentThread().getName());

        Runnable runnable = () -> System.out.println("This is another thread " + Thread.currentThread().getName());
        Thread newThread = new Thread(runnable);
        newThread.start();
        System.out.println("This is a current thread. Second print ln" + Thread.currentThread().getName());
    }
}
