public class Tasks {

    /**
     * Create a thread-safe wallet system.
     * You should be able to perform the following functions on the wallet system:
     * 1. Transfer between each wallet
     * 2. Get balance of each wallet
     * 3. Get the transfer records of each wallet
     *
     *
     *
     *
     * * Create an Employee Salary Management System
     *      * 1. Add new employee
     *      * 2. Set employee salary
     *      * 3. Employee should have JobRole
     *      * 4. Each JobRole should have salary and type of role
     *      * 5. You can only increase/decrease salary for the each role and once the salary of a role is increase,
     *      *every employee with that role would have the new amount as their salary
     *      * 6. fire employee
     *      * 7. calculate total salary you are paying to all active employees
     *      * 8. We should be able to see fired employees
     *      */


    /***
     * OOP
     *   -  Classes
     *          Abstract classes,
     *          abstract methods,
     *          inner classes(static and non-static),
     *          local classes,
     *          anonymous classes
     *   -  Interfaces
     *
     *   -  OOP
     *          Polymorphism
     *          Inheritance
     *          Composition
     *          Encapsulation
     *          Abstraction
     *
     *   -  Enumerated Types - [Predefined Objects]
     *   -  Collections
     *        Iterable (I)
     *           Collection (I)
     *              List (I)
     *                ArrayList, LinkedList, Vector
     *              Queue(I)
     *                Priority Queue
     *                Dequeue(I)
     *              Set(I)
     *                HashSet
     *                LinkedHashSet
     *                Sortedset(I)
     *   -  Map
     *   -  Contract (Equal & HashCode)
     *   -  Generic types
     *         Generic Class
     *         Generic methods
     *         Parameter Limit (wildcards usage with super, extends)
     *
     *   -  Concurrent/Parallel Programming
     *         Thread vs Process
     *         Synchronization
     *              Method Synchronization
     *              Block Synchronization
     *         Deadlock
     *         Communication between threads -  wait, notify()/notifyAll()
     *         Runnable(I)
     *         Callable(I)
     *         Future
     *         CompletableFuture
     *         ExecutorService
     *         Atomics
     *         Volatile - cache
     *
     *   -  Exception
     *         try
     *         catch
     *         finally
     *         Annotations
     *         Reflection APIs
     *
     *
     *   -  Functional Programming
     *      Functional Interfaces(supplier, consumer, function,UnaryOperator,Predicate)
     *      default
     *      lambda expression
     *      Optional
     *      Stream
     *   -  Java I/O
     *         I/O - Byte Streams
     *         I/O - Character Streams
     *         I/O - Data Buffering
     *
     *         NIO - Buffers and Channels
     *               Paths
     *
     *         Serialization
     *         Deserialization
     *         Serializable
     * */
}
