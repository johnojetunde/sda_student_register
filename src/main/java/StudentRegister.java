import java.util.ArrayList;
import java.util.List;

public class StudentRegister {
    private final List<String> register = new ArrayList<>();

    public void add(String studentName) {
        if (!isExist(studentName)) {
            register.add(studentName);
        }
    }

    private boolean isExist(String name) {
        return register.stream()
                .anyMatch(s -> s.equalsIgnoreCase(name));
    }

    private boolean isExistStudent(String name) {
        for (String student : register) {
            if (name.equalsIgnoreCase(student)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return register.size();
    }

    public List<String> get() {
        return register;
    }

    public void remove(String name) {
        register.remove(name);
    }
}
